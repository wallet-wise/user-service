using Microsoft.EntityFrameworkCore;
using UserService.Models.Entities;

namespace Core.Persistence;

public class UserServiceDbContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Address> Address { get; set; }

    public UserServiceDbContext(DbContextOptions<UserServiceDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<User>(entity =>
        {
            entity.HasKey(user => user.id);
            entity.Property(user => user.UserDetailsId).IsRequired(true);
        });

        builder.Entity<Address>(entity =>
        {
            entity.HasKey(address => address.id);
            entity.HasMany(address => address.Users)
                .WithOne(user => user.Address)
                .HasForeignKey(user => user.AddressId);
        });
    }
}