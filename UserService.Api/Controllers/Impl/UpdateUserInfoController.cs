using Microsoft.AspNetCore.Mvc;
using UserService.Api.Controllers.Contracts;
using UserService.Api.Dto;
using UserService.Api.Validation.Contracts;


namespace UserService.Api.Controllers.Impl;

[ApiController]
[Produces("application/json")]
[Route("api/users/updateuserinfo")]
public class UpdateUserInfoController : Controller<UpdateUserInfoRequest>, IUpdateUserInfoController
{
    protected override object perform(UpdateUserInfoRequest request)
    {
        return "hello";
    }

    [HttpPatch]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Handle(UpdateUserInfoRequest request)
    {
        var error = validate(request);
        if (error != null)
            return new BadRequestObjectResult(error);

        return new OkObjectResult(perform(request));
    }

    protected override List<IValidator> buildValidators(UpdateUserInfoRequest request)
    {
        var validators = new List<IValidator>();

        return validators;
    }
}