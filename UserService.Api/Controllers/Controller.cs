using Microsoft.AspNetCore.Mvc;
using UserService.Api.Validation;
using UserService.Api.Validation.Contracts;

namespace UserService.Api.Controllers;

public abstract class Controller<E>
{
    protected abstract object perform(E request);
    protected abstract List<IValidator> buildValidators(E request);

    protected string validate(E request)
    {
        List<IValidator> validators = new();
        return new ValidationComposite(validators).validate();
    }
}