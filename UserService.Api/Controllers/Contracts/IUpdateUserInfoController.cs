using Microsoft.AspNetCore.Mvc;
using UserService.Api.Dto;

namespace UserService.Api.Controllers.Contracts;

public interface IUpdateUserInfoController
{
    IActionResult Handle([FromBody] UpdateUserInfoRequest request);
}