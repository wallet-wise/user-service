using UserService.Api.Validation.Contracts;

namespace UserService.Api.Validation.Validators;

public class AbstractValidator : IValidator
{
    protected string fieldName { get; set; }
    protected object fieldValue { get; set; }

    public string Validate()
    {
        return null;
    }
}