using UserService.Api.Validation.Contracts;

namespace UserService.Api.Validation;

public class ValidationBuilder
{
    public string fieldName { get; set; }
    public object fieldValue { get; set; }
    public List<IValidator> Validators { get; set; }

    private ValidationBuilder(string fieldName, object fieldValue)
    {
        this.fieldValue = fieldValue;
        this.fieldValue = fieldValue;
    }

    public static ValidationBuilder of(string fieldName, object fieldValue)
    {
        return new ValidationBuilder(fieldName, fieldValue);
    }

    public List<IValidator> build()
    {
        return Validators;
    }
}