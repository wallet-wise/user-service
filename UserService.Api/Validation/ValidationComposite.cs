using UserService.Api.Validation.Contracts;

namespace UserService.Api.Validation;

public class ValidationComposite
{
    private List<IValidator> Validators;

    public ValidationComposite(List<IValidator> validators)
    {
        Validators = validators;
    }

    public string validate()
    {
        foreach (var validator in Validators)
        {
            var error = validator.Validate();
            if (error != null)
                return error;
        }

        return null;
    }
}