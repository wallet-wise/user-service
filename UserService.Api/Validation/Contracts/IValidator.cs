namespace UserService.Api.Validation.Contracts;

public interface IValidator
{
    string Validate();
}