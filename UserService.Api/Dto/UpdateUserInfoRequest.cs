using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace UserService.Api.Dto;

public class UpdateUserInfoRequest
{
    [Required] public string UserDetailsId;
    [Required] public string FirstName { get; set; }
    [Required] public string LastName { get; set; }
    [Required] public string Street { get; set; }
    [DefaultValue("S/N")] public string HouseNumber { get; set; }
    [DefaultValue("S/N")] public string Complement { get; set; }
    [Required] public string City { get; set; }
    [DefaultValue("S/N")] public string PostalCode { get; set; }
    [Required] public string Country { get; set; }
}