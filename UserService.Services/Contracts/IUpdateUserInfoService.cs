using UserService.Api.Dto;

namespace Services.Contracts;

public interface IUpdateUserInfoService
{
    string Update(UpdateUserInfoRequest request);
}