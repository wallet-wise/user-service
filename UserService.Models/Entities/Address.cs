using System.ComponentModel.DataAnnotations.Schema;

namespace UserService.Models.Entities;

[Table("t_address")]
public class Address
{
    public Guid id { get; set; }
    public string Street { get; set; }
    public string HouseNumber { get; set; }
    public string Complement { get; set; }
    public string City { get; set; }
    public string PostalCode { get; set; }
    public string Country { get; set; }
    public ICollection<User> Users { get; set; }
}