using System.ComponentModel.DataAnnotations.Schema;

namespace UserService.Models.Entities;

[Table("t_user")]
public class User
{
    public Guid id;
    public string UserDetailsId;
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public Guid AddressId;
    public Address Address { get; set; }
}