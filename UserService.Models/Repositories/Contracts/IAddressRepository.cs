using UserService.Models.Entities;

namespace UserService.Models.Repositories.Contracts;

public interface IAddressRepository
{
    User Save(User user);
}