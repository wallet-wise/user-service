using UserService.Models.Entities;

namespace UserService.Models.Repositories.Contracts;

public interface IUserRepository
{
    User Save(User user);
}